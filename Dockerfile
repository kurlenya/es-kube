FROM elasticsearch:5.0.1

# Install plugins
RUN /usr/share/elasticsearch/bin/elasticsearch-plugin install io.fabric8:elasticsearch-cloud-kubernetes:5.0.1 --verbose

# Override elasticsearch.yml config
COPY config/elasticsearch.yml ./config/elasticsearch.yml

RUN mkdir -p ./config/templates
COPY template-k8s-logstash.json ./config/templates/template-k8s-logstash.json

# Expose ports
EXPOSE 9200:9200
EXPOSE 9300:9300

# Set environment
ENV NAMESPACE default
ENV DISCOVERY_SERVICE elasticsearch-master
ENV CLUSTER_NAME elasticsearch-cluster
ENV NODE_MASTER true
ENV NODE_DATA true
ENV NODE_INGEST true
ENV HTTP_ENABLE true
	



